FROM python:3.8-slim-buster
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
RUN pip install prometheus-client
RUN pip install influxdb-client
RUN pip install psutil
COPY constant.py main.py ./
## RUN pip install pystrich
CMD [ "python3", "./main.py" ]