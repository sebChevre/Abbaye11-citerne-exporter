import constant
import requests
import json
from types import SimpleNamespace
from pymongo import MongoClient
from pprint import pprint
from twisted.internet import task, reactor
from datetime import datetime
import logging
import sys, os
#observability
import psutil #sudo pip install psutil

from dotenv import load_dotenv
#prometheus
from prometheus_client import start_http_server, Summary, Counter
import time
#influxdb
from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS



# Create a metric to track time spent and requests made.
TOTAL_PROCESS_TIME = Summary('process_processing_seconds', 'Time spent processing')
ITERATION_COUNTER = Counter('iteration_counter', 'Number of iterations')

# **************************** classes ****************************
class Volume(object):
    def __init__(self, response):
        self.actualVolume = response[0]['ActualVolume']
        self.actualVolumePercent = response[0]['ActualVolumePercent']
        self.battery = response[0]['Battery']
        self.lastUpdate = response[0]['LastUpdate']
        

class DeviceResponse(object):
    def __init__(self, DeviceResponse):
        self.DeviceResponse = Volume(DeviceResponse)

        
# **************************** fonctions ****************************
def initMongoDbConnection():
    global CONNECTION_STR
    
    if(MONGODB_PROTOCOL_SCHEME == 'mongodb+srv'):

        MONGODB_USERNAME = os.getenv('MONGODB_USERNAME')
        if(MONGODB_USERNAME is None):
            logger.error("Mongodb username must be set with mongodb+srv scheme,applicaton will exit")
            sys.exit(0)

        logger.debug('Env variable MONGODB_USERNAME defined: {0}'.format(MONGODB_USERNAME))

        MONGODB_PASSWORD = os.getenv('MONGODB_PASSWORD')
        if(MONGODB_USERNAME is None):
            logger.error("Mongodb password must be set with mongodb+srv scheme,applicaton will exit")
            sys.exit(0)
        logger.debug('Env variable MONGODB_PASSWORD defined: {0}'.format("******"))

        CONNECTION_STR = f'{MONGODB_PROTOCOL_SCHEME}://{MONGODB_USERNAME}:{MONGODB_PASSWORD}@{MONGODB_HOST}'
        logger.debug('MONGODB CONNECTION STRING: {0}'.format(CONNECTION_STR))

    elif (MONGODB_PROTOCOL_SCHEME == 'mongodb'):
        CONNECTION_STR = f'{MONGODB_PROTOCOL_SCHEME}://{MONGODB_HOST}:{MONGODB_PORT}'
        logger.debug('MONGODB CONNECTION STRING: {0}'.format(CONNECTION_STR))
        # TODO Credentials
        # connection_str = f'{MONGODB_PROTOCOL_SCHEME}://{MONGODB_USERNAME}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}'
    else:
        logger.error('Mongodb protocol scheme is invalid: {0} - application will exit'.format(MONGODB_PROTOCOL_SCHEME))
        sys.exit(0)
    

def pushToInflux():
    client = InfluxDBClient(url=INFLUX_DB_CLIENT_URL, token=INFLUX_DB_TOKEN, org=INFLUX_DB_ORGS)
    
    write_api = client.write_api(write_options=SYNCHRONOUS)

    #write cpu
    cpuPercent = psutil.cpu_percent(4)
    point = Point("cpu") \
        .tag("app", INFLUX_DB_BUCKET) \
        .field("used_percent", cpuPercent) \
        .time(datetime.utcnow(), WritePrecision.NS)
    write_api.write(INFLUX_DB_BUCKET, INFLUX_DB_ORGS, point)

    #write ram
    ramPercent = psutil.virtual_memory()[2]
    point = Point("ram") \
        .tag("app", INFLUX_DB_BUCKET) \
        .field("used_percent", ramPercent) \
        .time(datetime.utcnow(), WritePrecision.NS)

    write_api.write(INFLUX_DB_BUCKET, INFLUX_DB_ORGS, point)


def insertVolumeReports(report):
    client = MongoClient(CONNECTION_STR)
    db = client.home
    result=db.citerne.insert_one(
        {'reportDate':datetime.now().strftime("%d/%m/%Y %H:%M:%S"),
        'actualVolume':report.actualVolume,
        'actualVolumePercent':report.actualVolumePercent,
        'battery':report.battery, 
        'lastUpdate':report.lastUpdate})
    logger.info('Volume report successfuly created, id: {0},'.format(result.inserted_id))
    

def initEnv():
    
    global CITERNE_SERVICE_ENDPOINT_URL, MONGODB_CONNECTION_STRING, EXECUTION_DELAY, MONGODB_USERNAME
    global MONGODB_PASSWORD, MONGODB_HOST, MONGODB_PORT, MONGODB_PORT,MONGODB_PROTOCOL_SCHEME 
    global INFLUX_DB_CLIENT_URL,INFLUX_DB_TOKEN,INFLUX_DB_ORGS,INFLUX_DB_BUCKET

    CITERNE_SERVICE_ENDPOINT_URL = os.getenv('CITERNE_SERVICE_ENDPOINT_URL',default=constant.CITERNE_SERVICE_ENDPOINT_URL)
    logger.debug('Env variable CITERNE_SERVICE_ENDPOINT_URL defined: {0}'.format(CITERNE_SERVICE_ENDPOINT_URL))

    EXECUTION_DELAY = int(os.getenv('EXECUTION_DELAY',default=constant.EXECUTION_DELAY))
    logger.debug('Env variable EXECUTION_DELAY: {0}'.format(EXECUTION_DELAY))
    
    MONGODB_HOST = os.getenv('MONGODB_HOST',default=constant.MONGODB_HOST)
    logger.debug('Env variable MONGODB_HOST defined: {0}'.format(MONGODB_HOST))

    MONGODB_PORT = os.getenv('MONGODB_PORT',default=constant.MONGODB_PORT)
    logger.debug('Env variable MONGODB_PORT defined: {0}'.format(MONGODB_PORT))

    MONGODB_PROTOCOL_SCHEME = os.getenv('MONGODB_PROTOCOL_SCHEME',default=constant.MONGODB_PROTOCOL_SCHEME)
    logger.debug('Env variable MONGODB_PROTOCOL_SCHEME defined: {0}'.format(MONGODB_PROTOCOL_SCHEME))

    INFLUX_DB_CLIENT_URL = os.getenv('INFLUX_DB_CLIENT_URL',default=constant.INFLUX_DB_CLIENT_URL)
    logger.debug('Env variable INFLUX_DB_CLIENT_URL defined: {0}'.format(INFLUX_DB_CLIENT_URL))

    INFLUX_DB_TOKEN = os.getenv('INFLUX_DB_TOKEN',default=constant.INFLUX_DB_TOKEN)
    logger.debug('Env variable INFLUX_DB_TOKEN defined: {0}'.format(INFLUX_DB_TOKEN))

    INFLUX_DB_ORGS = os.getenv('INFLUX_DB_ORGS',default=constant.INFLUX_DB_ORGS)
    logger.debug('Env variable INFLUX_DB_ORGS defined: {0}'.format(INFLUX_DB_ORGS))

    INFLUX_DB_BUCKET = os.getenv('INFLUX_DB_BUCKET',default=constant.INFLUX_DB_BUCKET)
    logger.debug('INFLUX_DB_BUCKET: {0}'.format(INFLUX_DB_BUCKET))


def initLogger():
    global logger

    logLevel = os.getenv('LOG_LEVEL',default="INFO")

    root = logging.getLogger()
    root.setLevel(logLevel)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logLevel)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)
    logger = logging.getLogger('main.py')


@TOTAL_PROCESS_TIME.time()
def mainLoop():
    
    try:
        response = requests.post(
            CITERNE_SERVICE_ENDPOINT_URL, 
            json=constant.CITERNE_SERVICE_POST_BODY,
            headers=constant.CITERNE_SERVICE_HEADERS
        )
    
        logger.info("Request ok, status code 200")
        logger.debug("response: %s",response.json())
        jsonResp = json.loads(response.json())
        resp = DeviceResponse(**jsonResp)
        insertVolumeReports(resp.DeviceResponse)
        ITERATION_COUNTER.inc()
    except requests.ConnectionError as error:
        logger.error(error)
    except requests.exceptions.HTTPError as error:
        logger.error(error)
        

def observabilityLoop () :

    cpuPercent = psutil.cpu_percent(4)
    ramPercent = psutil.virtual_memory()[2]

    logger.debug("[PSUTIL] - CPU usage[%] {0} used".format(cpuPercent))
    logger.debug("[PSUTIL] - RAM memory[%] {0} used".format(ramPercent))

    pushToInflux()


### MAIN EXECUTION
load_dotenv()
initLogger()
initEnv()
initMongoDbConnection()
start_http_server(8000)

logger.info("Starting observabilityLopp. Collect system data each %d seconds",10)
observabilityThread = task.LoopingCall(observabilityLoop)
observabilityThread.start(10) 

logger.info("Starting mainLoop. Polling citerne service each %d seconds",EXECUTION_DELAY)
mainThread = task.LoopingCall(mainLoop)
mainThread.start(EXECUTION_DELAY) 

 
reactor.run()
